#ifndef KILLPOSTE_H
#define KILLPOSTE_H

#ifndef linux
#ifndef MSDOS
#error "Vous devez definir le systeme d'exploitation linux ou MSDOS."
#endif
#endif

#ifdef linux
#define UNIX
#define MYOS "linux"                    /* Pour affichage version only    */
#else
#define MYOS "MSDOS"
#endif


#define DICT_PATH "/usr/share/dico-cougnenc"  /* path par defaut */
#define DOS_PATH  "lexique"              /* path par defaut Dos uniquement  */
#define DICT_EXT ".dic"                  /* extension des fichiers dicos    */
#define VERSION_FILE "version.dic"       /* fichier texte version dico      */

#define ERROR 1
#define OK    0
#define MYVBUF 10240                     /* pour setvbuf() eventuel         */
#define COMMENT_CHAR  35                 /* ligne de commentaire: '#'       */

#ifndef linux
#define MUST_HAVE_BUF                  /* Linux va moins vite avec setvbuf! */
#endif

#ifdef UNIX 
#define TMPPATH "/tmp"
#define READ_BINARY  "r"
#define WRITE_BINARY "w"
#define READ_TEXT    "r"
#else
#define TMPPATH ""
#define READ_BINARY  "rb"
#define WRITE_BINARY "wb"
#define READ_TEXT    "rt"
#endif

typedef unsigned char byte;

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#ifdef MSDOS
void GetDosPath(char *path);
void TakePath  (char *path);
#endif

/* Affiche le nom et numero de version du programme */
void mybanner();

/* Ecran d'acceuil, demande de confirmation */
int do_not();

/* Passe en revue toutes les lignes du dictionnaire, et
 * elimine les lignes comportant des codes postaux ... */
int cleandic();

/* Supprime un line feed et/ou un retour charriot */
void nocrlf( char *str);

/* isposte(buf) retourne 1 si la ligne 'buf' est un code postal */
int isposte( unsigned char *buf );

/* message d'erreur et explications sur la recherche 
 * du path du dictionnaire. */
int file_usage();

/* Recherche du lexique specifique a MS-DOS */
void GetDosPath(char *path);
void TakePath(char *path);

/* only for MS-DOS ?? */
int getpid();
void unlink();

#endif /* KILLPOSTE_H */
