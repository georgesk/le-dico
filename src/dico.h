#ifndef DICO_H
#define DICO_H

#ifndef linux
#ifndef sony
#ifndef MSDOS
   #error "Vous devez definir le systeme d'exploitation linux,sony ou MSDOS."
#endif
#endif
#endif

#ifdef sony                             /* Sony News WorkStations        */
#define UNIX
#define ISO_CHARS
#define MYOS "SONY"                     /* Pour affichage version only.  */
char *getenv();
#endif

#ifdef linux                            /* Linux Operating System        */ 
#define UNIX
#define ISO_CHARS
#define MYOS "linux" 
#endif

#ifdef MSDOS                            /* Systeme d'operation de disque */
#define MYOS "MSDOS"                    /* Microsoft :-)                 */
#endif

#define IBM_TERM    1                   /* Pour translation affichage      */
#define ASCII_TERM  2
#define ISO_TERM    3
#define COMMENT_CHAR 35                 /* '#' ligne de commentaire        */

#define DICT_PATH "/usr/share/dico-cougnenc"  /* path par defaut unix/dos        */
#define DOS_PATH  "lexique"              /* path par defaut Dos uniquement  */
#define DICT_EXT ".dic"                  /* extension des fichiers dicos    */
#define VERSION_FILE "version.dic"       /* fichier texte version dico      */

#define ERROR 1
#define OK    0
#define MYVBUF 10240                     /* pour setvbuf() eventuel         */

#ifndef linux
#ifndef sony
#define MUST_HAVE_BUF                  /* Linux va moins vite avec setvbuf! */
#endif
#endif

typedef unsigned char byte;

#include <stdio.h>
#include <string.h>
#ifndef sony
#include <stdlib.h>
#endif

//Ajout
#ifdef linux
#include <locale.h>
#include <langinfo.h>
#include <iconv.h>
#endif

/* Message d'aide generale. */
int usage();

/* message d'erreur et explications */
int file_usage();

/* Traite les options speciales de la ligne de commande */
int options( char *c );

/* compte le nombre de mots dans le dico actuel */
int countwords();

/* Recherche dans le fichier de l'initiale ... */
void lookfor ( char *pattern );

/* Recherche dans TOUS les 26 fichiers */
void lookall ( char *pattern );

/* Affiche le nom et numero de version du programme */
void mybanner();

/* Regarde s'il faut afficher en ISO, IBM ou ASCII pur */
void translate( char * buf);

/* Modifie une chaine pour la rendre plus lisible */
byte * beautify ( byte * str );

/* Retourne 0 si le caractere n'est pas un truc pour 
 * un debut d'expressions regulieres. */
int isjoker( int c );

/* Supprime un line feed a la fin d'une chaine */
void nolf( char *str);

/* Gestion simplifiee des expressions regulieres */
int match(char *_fname, char *_pat);
int fmatch(char *_fname, char *_pat);

/* special tolower 8bits, don't touch 8 bit chars */
byte ToLower (byte c );

/* Special strlwr routine, works whith 8 bit chars ISO/IBM */
byte *StrLwr( byte *str );

/* special toupper 8bits, don't touch 8 bit chars */
byte ToUpper (byte c );

/* Special strupr routine, works whith 8 bit chars ISO/IBM */
byte *StrUpr( byte *str );

/* Translates an ISO accentuated string to an MS-DOS one. */
void StrIBM ( byte *str );

/* Translates an MS-DOS accentuated string to a ISO one */
void StrISO ( byte *str );

/* Convertit l'encodage d'une chaine de caractères donnés
 * à partir d'un encodage vers un autre */
char * conversion_charset (char* s, char* from_charset, char* to_charset);

/* Translates iso accentuated chars to IBM ones */
void iso2ibm( unsigned char *c );

/* Translates ibm accentuated chars by ISO char */
void ibm2iso( unsigned char *c );

/* Renvoie le caractere non accentue equivalent a une lettre accentuee,
 * en Francais */
byte equival( byte ch );

/* Supprime les accents Francais dans une chaine, la rendant pur 7 bits */
byte* StrAscii ( byte *str);

/* Recherche du lexique specifique a MS-DOS */
void GetDosPath(char *path);
void TakePath(char *path);

#endif /* DICO_H */
