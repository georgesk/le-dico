DESTDIR =
SUBDIRS = src manuel

all:
	for d in $(SUBDIRS); do $(MAKE) -C $$d $@ DESTDIR=$(DESTDIR); done

clean:
	for d in $(SUBDIRS); do $(MAKE) -C $$d $@ DESTDIR=$(DESTDIR); done

install:
	for d in $(SUBDIRS); do $(MAKE) -C $$d $@ DESTDIR=$(DESTDIR); done

.PHONY: all clean install
